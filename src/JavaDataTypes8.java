public class JavaDataTypes8 {
	public static void main(String[] args) {
		boolean isJavaFun = true;
		boolean isFishTasty = false;
		System.out.println(isJavaFun); // Output true
		System.out.println(isFishTasty); // Output false
	}
}
