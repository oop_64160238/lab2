public class JavaOperators2 {
	public static void main(String[] args) {
		int sum1 = 100 + 50; // 150 (100 + 50)
		int sum2 = sum1 + 150; // 300 (150 + 150)
		int sum3 = sum2 + sum2; // 600 (300 + 300)
		System.out.println(sum3);
	}
}
